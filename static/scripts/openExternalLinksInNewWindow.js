(function(global){
    "use strict";

    /**
     * Open all external links in a new window
     */
    function openExternalLinksInNewWindow() {
        var host = window.location.host;
        var usingLocalFiles = (window.location.protocol === "file:");

        document.querySelectorAll('a').forEach(function(elm){
            var isLocalFile = (elm.protocol === "file:");
            var href = elm.href;
            var regex = new RegExp("^(http(s)?:)?//(?!"+host+")", 'i');

            if ((usingLocalFiles && !isLocalFile) || href.match(regex)) {
                console.debug("external", href, elm);
                elm.classList.add("external-link");
                elm.setAttribute("target", "_blank");
            }
        });
    }

    global.openExternalLinksInNewWindow = openExternalLinksInNewWindow;
})(this);