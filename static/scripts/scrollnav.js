function scrollNav() {
    var navElm = document.querySelector('body > nav');
    var links = navElm.querySelectorAll('li > a');
    var path = location.protocol + "//" + location.pathname;
    var fullPath = location.href;
    var idx, link, firstActiveLink;

    for (idx = 0; idx < links.length; idx++) {
        link = links[idx];

        link.classList.remove("active");

        if (link.href == path) {
            link.classList.add("active");
        } else if (link.href == fullPath) {
            link.classList.add("active");
        }
    }

    firstActiveLink = navElm.querySelector('li > a.active');
    if (firstActiveLink) {
        navElm.scrollTo(0, firstActiveLink.offsetTop);
    }
}
